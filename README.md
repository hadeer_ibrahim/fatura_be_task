# Fatura Backend Task


Challenge brief
---

Implement a Restful APIs for Authentication cycle (login, register, and logout) also API For list active sessions and revoke it by id using laravel passport. However, the APIs validate if the user has the right permissions to do this action applied on the CRUD for roles module which can be easly extends to the whole system. 
  

---

### What had I done:

- [x] ***service is responsible for authenticate and login users***
  I implemented 2 APIs in Authentication for register users to system, and login authenticated users into system.

- [x] ***force invalidating sessions*** 
  I implemented 2 APIs under Authentication layer to list all active sessions for current authenticated user and, revoke session by its id. 

- [x] ***service is responsible for logging users out from the system***
  I implemented 1 APIs under Authentication layer for logout users from system.
  
- [x] ***service is responsible for validating whether logged user is permitted to do specific action or not***
  I implemented 4 APIs in Role Module.

- [x] ***How I identify and secure user's session (session, jwt)***
  I used passport to handle secure user session. 

- [x] ***I structure my roles and permissions***
  to not invent the wheel again I used a package called [spatie/laravel-permission](https://spatie.be/docs/laravel-permission/v5/introduction).
  build tables and handle permission assigment to role

- [x] ***how to assign specific user a specific role or permission***
  by creating seeder for listing permission like `create-role` , `list-role` and `view-role`
  then creating another seeder to create user and assign role to him then assign permission to user.

- [x] Build **Unit Test** Using ***PHPUNIT***
    - in Auth Module to test login in success case.
    - in Auth Module to test failure by invalid email case.
    - in Auth Module to test failure by password case.
    - in Auth Module to test register to the system.

---

### How to Run The Code

- download the code repo.
- please self host the code at any apache server (version 2.4).
- run command `php artisan serve` to make server up.
- run command `composer install` to install needed
  package ( [laravel modules](https://nwidart.com/laravel-modules/v6/introduction) - 
  [laravel passport](https://laravel.com/docs/8.x/passport) -
  [spatie/laravel-permission](https://spatie.be/docs/laravel-permission/v5/introduction)).
- run command `php artisan migrate` to build database.
- run command `php artisan passport:install` to install passport.
- run command `php artisan module:seed Role --class=PermissionTableSeeder` to insert list of permission in role module
- run command `php artisan module:seed Role --class=CreateAdminUserSeeder` to create admin role and assign permission to this role
- run command `./vendor/bin/phpunit --filter=AuthTestFeature` to run test cases
  


##### Postman Collection
- However, here is a [Postman Collection](./fatura.postman_collection.json)


##### APIs Usage
- `***/api/register` to register user into system.
- `***/api/login` to login authenticated user into system.
- `***/api/logout` to log user out of the system.
- `***/api/active-sessions` to list all active session to current authenticated user.
- `***/api/session-revoke/:sessionId` to force invaliding session By Id.
- `***/api/roles` to list all roles in the system --Get method.
- `***/api/roles` to create a new role with permissions ids system --POST method.
- `***/api/roles/:id` to update role with permissions ids system --PUT method.
- `***/api/roles/:id` to view role in the system --Get method.

---

### Trade-offs
- I always used to have unit test for using **PHPUNIT**, but I do not have the time to cover all test cases need for all apis.
- If I had much time for the task, ***Refresh token mechanism*** to handle this point _how to keep user's session valid_
