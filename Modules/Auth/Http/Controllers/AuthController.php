<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Auth\Http\Requests\LoginRequest;
use Modules\Auth\Http\Requests\RegisterRequest;
use Modules\Auth\Services\AuthService;

class AuthController extends Controller
{
    /**
     * @var AuthService
     */
    private $service;

    /**
     * AuthController constructor.
     * @param AuthService $service
     */
    public function __construct(AuthService $service)
    {
        $this->service = $service;
        $this->middleware('auth:api')->only(['logout', 'listActiveSessions', 'forceInvalidSession']);
    }

    /**
     *  register user to system
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        try {
            $createdUser = $this->service->createUser($request->all());
            return response()->json(['data' => $createdUser], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }

    /**
     * login and validate user credential
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try {
            $validUser =  $this->service->loginUser($request->all());

            return response()->json(['data' => $validUser], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }

    /**
     * logout user from system
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        return response( ['message' => 'You have been successfully logged out!'], Response::HTTP_OK);
    }


    /**
     * list active Sessions
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function listActiveSessions(): \Illuminate\Http\JsonResponse
    {
        try {
           $sessions = $this->service->getActiveSessions();
           return response()->json(['data' => $sessions], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function forceInvalidSession(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        try {
            $InvalidSession = $this->service->RevokeActiveSessionById($id);

            return response()->json(['data' =>[$InvalidSession] ], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }
}
