<?php


namespace Modules\Auth\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthRepository
{

    /**
     * @var User
     */
    private $model;

    /**
     * AuthRepository constructor.
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $userData
     * @return array
     */
    public function create(array $userData): array
    {
        $user = User::create($userData);
        $accessToken = $user->createToken('authToken')->accessToken;

        return [ 'user' => $user, 'access_token' => $accessToken];
    }

    /**
     * @return mixed
     */
    public function createToken()
    {
        return auth()->user()->createToken('authToken')->accessToken;
    }


    public function listActiveSessions($userId)
    {
       $data = \DB::table('oauth_access_tokens')
            ->where('user_id', $userId)
            ->where('revoked' , false)->get();

       return $data;
    }

    public function RevokeSession($sessionId)
    {
       $session = \DB::table('oauth_access_tokens')
            ->where('user_id', Auth::id())
            ->where('id', $sessionId)
            ->where('revoked', false)->get();

      if ($session != null) {
          \DB::table('oauth_access_tokens')
              ->where('user_id', Auth::id())
              ->where('id', $sessionId)
              ->update(['revoked' => true]);
      }
      return $session;
    }

}
