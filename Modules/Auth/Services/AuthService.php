<?php


namespace Modules\Auth\Services;



use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Auth\Repositories\AuthRepository;

class AuthService
{
    /**
     * @var AuthRepository
     */
    private $repository;

    /**
     * AuthService constructor.
     * @param AuthRepository $repository
     */
    public function __construct(AuthRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param array $userData
     * @return array
     */
    public function createUser(Array $userData): array
    {
       $userData['password'] = bcrypt($userData['password']);
       return $this->repository->create($userData);
    }

    /**
     * @param array $loggedUserData
     * @return array
     * @throws \Exception
     */
    public function loginUser(Array $loggedUserData): array
    {
        if (!auth()->attempt($loggedUserData)) {
            throw  new \Exception('Invalid Credentials',Response::HTTP_UNAUTHORIZED);
        }

        $accessToken = $this->repository->createToken();

        return ['user' => auth()->user(), 'access_token' =>  $accessToken];
    }


    /**
     * @return mixed
     */
    public function getActiveSessions()
    {
        $userId = Auth::id();

        return $this->repository->listActiveSessions($userId);
    }

    /**
     * @return mixed
     */
    public function RevokeActiveSessionById($id)
    {


        return $this->repository->RevokeSession($id);
    }
}
