<?php

namespace Modules\Auth\Tests\Feature;

use App\Models\User;
use Tests\TestCase;
class AuthTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * test register api
     */
    public function testRegister() {
        $user = User::factory()->make();
        $body = [
            'name' => $user->name,
            'email' => $user->email,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

       $this->json('POST','/api/register', $body, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data'=>['user','access_token']]);
    }

    /**
     * test login api
     */
    public function testLogin() {
        $user = User::factory()->create();
        $body = [
            'email' => $user->email,
            'password' => 'password'
        ];

        $this->json('POST','/api/login',$body,['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure(['data'=>['user','access_token']]);
    }

    /**
     * test failure login api
     */
    public function testLoginInvalidEmail() {
        $user = User::factory()->create();

        $body = [
            'email' => 'test1@example.com',
            'password' => 'password'
        ];

        $this->json('POST','/api/login',$body,['Accept' => 'application/json'])
            ->assertStatus(422);
    }
    /**
     * test failure login api
     */
    public function testLoginInvalidPassword() {
        $user = User::factory()->create();

        $body = [
            'email' => $user->email,
            'password' => 'password123'
        ];

      $this->json('POST','/api/login',$body,['Accept' => 'application/json'])
            ->assertStatus(401);
    }
}
