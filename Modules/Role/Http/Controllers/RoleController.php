<?php

namespace Modules\Role\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Role\Http\Requests\CreateRoleRequest;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:role-list|role-create|role-edit|role-delete', ['only' => ['index','store']]);
        $this->middleware('permission:role-create')->only('store');
        $this->middleware('permission:role-edit')->only('update');
        $this->middleware('permission:role-delete')->only('destroy');
    }

    /**
     * list all Roles
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(): \Illuminate\Http\JsonResponse
    {
        try {
            $roles = Role::all();
            return response()->json(['data' => $roles], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }


    /**
     * @param CreateRoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRoleRequest $request): \Illuminate\Http\JsonResponse
    {
        try {
            $role = Role::create(['name' => $request->input('name')]);
            $role->syncPermissions($request->input('permission'));
            return response()->json(['data' =>['role' => $role, 'permissions' => $role->permissions] ], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id): \Illuminate\Http\JsonResponse
    {
        try {
            $role = Role::findOrFail($id);
            $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
                ->where("role_has_permissions.role_id",$id)
                ->get();

            return response()->json(['data' =>['role' => $role, 'permissions' => $rolePermissions] ], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }

    /**
     * update specific role
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        try {
            $role = Role::findOrFail($id);
            $role->name = $request->input('name');
            $role->save();

            $role->syncPermissions($request->input('permission'));
            return response()->json(['data' =>['role' => $role, 'permissions' => $role->permissions] ], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }

    /**
     * delete specific role
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        try {
            $role = Role::findOrFail($id);
            $role->where('id',$id)->delete();
            return response()->json(['message' => 'Role is deleted Successfully'], Response::HTTP_OK);
        }catch (\Exception $exception){
            return response()->json(['message' => $exception->getMessage()], $exception->getCode());

        }
    }
}
