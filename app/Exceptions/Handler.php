<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {

        });
    }

    public function report(Throwable $e)
    {
        parent::report($e);

    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof BadRequestHttpException) {
            return response()->json([
                "message" => $exception->getMessage()
            ],Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof AccessDeniedHttpException) {

            return response()->json([
                "message" => "unauthorized"
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof UnauthorizedException) {

            return response()->json([
                "message" => "unauthorized"
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->json([
                "message" => "not found"
            ], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof ModelNotFoundException) {
            return response()->json([
                "message" => "model not found"
            ], Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->json([
                "message" => "method not allowed"
            ], Response::HTTP_METHOD_NOT_ALLOWED);
        }

        if ($exception instanceof TooManyRequestsHttpException) {
            return response()->json([
                "message" => "to many request"
            ], Response::HTTP_TOO_MANY_REQUESTS);
        }
        return parent::render($request, $exception);

    }
}
